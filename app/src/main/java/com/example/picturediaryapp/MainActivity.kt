package com.example.picturediaryapp


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private val _helper = DatabaseHelper(this@MainActivity)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rvDiary = findViewById<RecyclerView>(R.id.rvDiary)
        val layout = LinearLayoutManager(applicationContext)
        rvDiary.layoutManager = layout
        val diaryList = createDiaryList()
        val adapter = RecyclerListAdapter(diaryList)
        rvDiary.adapter = adapter
        val decorator = DividerItemDecoration(applicationContext, layout.orientation)
        rvDiary.addItemDecoration(decorator)
    }

    override fun onDestroy() {
        _helper.close()
        super.onDestroy()
    }

    private fun createDiaryList(): MutableList<MutableMap<String, String>>{
        val diaryList: MutableList<MutableMap<String, String>> = mutableListOf()
        var diary = mutableMapOf("date" to "2020/04/12", "title" to "ももちきた")
        diaryList.add(diary)
        diary = mutableMapOf("date" to "2020/04/10", "title" to "友達の島に")
        diaryList.add(diary)
        diary = mutableMapOf("date" to "2020/04/08", "title" to "こんにちは")
        diaryList.add(diary)
        diary = mutableMapOf("date" to "2020/04/05", "title" to "無人島生活開始")
        diaryList.add(diary)
        return diaryList

    }

    private inner class RecyclerListViewHolder(itemView: View) : RecyclerView.ViewHolder(
        itemView) {
        var tvDate: TextView
        var tvTitle: TextView

        init {
            tvDate = itemView.findViewById(R.id.tvDate)
            tvTitle = itemView.findViewById(R.id.tvTitle)
        }

    }

    private inner class RecyclerListAdapter(private val _listData: MutableList<MutableMap<
            String, String>>): RecyclerView.Adapter<RecyclerListViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerListViewHolder {
            val inflater = LayoutInflater.from(applicationContext)
            val view = inflater.inflate(R.layout.row, parent, false)
            view.setOnClickListener(ItemClickListener())
            val holder = RecyclerListViewHolder(view)
            return holder
        }

        override fun onBindViewHolder(holder: RecyclerListViewHolder, position: Int) {
            val item = _listData[position]
            val diaryDate = item["date"] as String
            val diaryTitle = item["title"] as String
            holder.tvDate.text = diaryDate
            holder.tvTitle.text = diaryTitle
        }

        override fun getItemCount(): Int {
            return _listData.size
        }
    }

    private inner class ItemClickListener : View.OnClickListener {
        override fun onClick(view: View) {

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.diary_list_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val intent = Intent(applicationContext, editDiary::class.java)
        startActivity(intent)
        return super.onOptionsItemSelected(item)
    }
}
