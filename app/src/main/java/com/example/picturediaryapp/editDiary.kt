package com.example.picturediaryapp

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.SurfaceView
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_edit_diary.*
import java.time.Month
import java.time.Year

class editDiary : AppCompatActivity(){

//    private var _picturediaryId = -1

    private val _helper = DatabaseHelper(this@editDiary)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_diary)

        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        toolbar.setTitle(R.string.toolbar_title)
        toolbar.setTitleTextColor(Color.WHITE)
        setSupportActionBar(toolbar)

        /// CustomSurfaceViewのインスタンスを生成しonTouchリスナーをセット
        val customSurfaceView = CustomSurfaceView(this, sVPicture)
        sVPicture.setOnTouchListener { v, event ->
            customSurfaceView.onTouch(event)
        }

//        val db = _helper.writableDatabase
//        val sql = "SELECT * FROM picturediarys WHERE _id = ${_picturediaryId}"
//        val cursor = db.rawQuery(sql, null)
//
//        var title = ""
//        while (cursor.moveToNext()) {
//            val idxTitle = cursor.getColumnIndex("title")
//            title = cursor.getString(idxTitle)
//        }
//        val dST = findViewById<EditText>(R.id.dialogSaveTitle)
//        dST.setText(title)
//
//        var year = ""
//        while (cursor.moveToNext()) {
//            val idxYear = cursor.getColumnIndex("year")
//            year = cursor.getString(idxYear)
//        }
//        val dSY = findViewById<EditText>(R.id.dialogSaveYear)
//        dSY.setText(year)
//        val tvYear = findViewById<TextView>(R.id.tvYear)
//        tvYear.setText(year)
//
//        var month = ""
//        while (cursor.moveToNext()) {
//            val idxMonth = cursor.getColumnIndex("month")
//            month = cursor.getString(idxMonth)
//        }
//        val dSM = findViewById<EditText>(R.id.dialogSaveMonth)
//        dSM.setText(month)
//        val tvMonth = findViewById<TextView>(R.id.tvMonth)
//        tvMonth.setText(month)
//
//        var day = ""
//        while (cursor.moveToNext()) {
//            val idxDay = cursor.getColumnIndex("day")
//            day = cursor.getString(idxDay)
//        }
//        val dSD = findViewById<EditText>(R.id.dialogSaveDay)
//        dSD.setText(day)
//        val tvDay = findViewById<TextView>(R.id.tvDay)
//        tvDay.setText(day)
//
//        var note = ""
//        while (cursor.moveToNext()) {
//            val idxNote = cursor.getColumnIndex("note")
//            note = cursor.getString(idxNote)
//        }
//        val eVDiary = findViewById<EditText>(R.id.eVdiary)
//        eVDiary.setText(note)

//        var picture = ""
//        while (cursor.moveToNext()) {
//            val idxPicture = cursor.getColumnIndex("picture")
//            picture = cursor.getBlob(idxPicture)
//        }
//        val sVPicture = findViewById<SurfaceView>(R.id.sVPicture)
//        sVPicture.sets(picture)
    }

    override fun onDestroy() {
        _helper.close()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.diaryListOptionSave ->
                DiarySaveDialogFragment().show(supportFragmentManager, "DiarySaveDialogFragment")
            R.id.diaryListOptionDelete ->
                DiaryDeleteDialogFragment().show(
                    supportFragmentManager,
                    "DiaryDeleteDialogFragment"
                )
            R.id.dialyColorParret ->
                ParretDialogFragment().show(supportFragmentManager, "ParretDialogFragment")
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.diary_options_menu_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

//    ペンの色変更
//    override fun onClick(v : View){
//        val sVPicture = findViewById<SurfaceView>(R.id.sVPicture)
//        when(v.id){
//            R.id.vff000000 -> sVPicture.()
//            R.id.vff000080 -> sVPicture.()
//
//        }
//    }

    fun onSaveButtonClick(view: View) {
        val dST = findViewById<EditText>(R.id.dialogSaveTitle)
        val dSY = findViewById<EditText>(R.id.dialogSaveYear)
        val dSM = findViewById<EditText>(R.id.dialogSaveMonth)
        val dSD = findViewById<EditText>(R.id.dialogSaveDay)
        val eVDiary = findViewById<EditText>(R.id.eVdiary)
//        val sVPicture = findViewById<SurfaceView>(R.id.sVPicture)

        val title = dST.text.toString()
        val year = dSY.text.toString()
        val month = dSM.text.toString()
        val day = dSD.text.toString()
        val note = eVDiary?.text.toString()
//        val picture = sVPicture?.holder.toString()

        val db = _helper.writableDatabase

        val sqlDelete = "DELETE FROM picturediarys WHERE _id = ?"
        var stmt = db.compileStatement(sqlDelete)
        //変数のバイド
        stmt.executeUpdateDelete()

        val sqlInsert = "INSERT INTO picturediarys (_id, title, year, month, day, note, picture) VALUES (?, ?, ?, ?, ?, ?, ?)"
        stmt = db.compileStatement(sqlInsert)
        //変数のバイド
        stmt.executeInsert()

        finish()
    }


}
