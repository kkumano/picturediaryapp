package com.example.picturediaryapp

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Bitmap
import android.os.Bundle
import android.view.SurfaceView
import android.view.View
import android.widget.EditText
import android.widget.NumberPicker
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.activity_edit_diary.*
import kotlinx.android.synthetic.main.dialog_save.*
import java.text.SimpleDateFormat
import java.util.*

class DiarySaveDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val SaveView = inflater.inflate(R.layout.dialog_save, null)

        val dSY = SaveView.findViewById<EditText>(R.id.dialogSaveYear)
        dSY.setHint(getTodayYear())
        val dSM = SaveView.findViewById<EditText>(R.id.dialogSaveMonth)
        dSM.setHint(getTodayMonth())
        val dSD = SaveView.findViewById<EditText>(R.id.dialogSaveDay)
        dSD.setHint(getTodayDay())

        builder.setView(SaveView)
            .setTitle("更新日")
            .setPositiveButton("保存") { dialog, id ->
                //データベースへの保存、変更

//                val dST = SaveView.findViewById<EditText>(R.id.dialogSaveTitle)
//                val dSY = SaveView.findViewById<EditText>(R.id.dialogSaveYear)
//                val dSM = SaveView.findViewById<EditText>(R.id.dialogSaveMonth)
//                val dSD = SaveView.findViewById<EditText>(R.id.dialogSaveDay)
//                val eVDiary = activity?.findViewById<EditText>(R.id.eVdiary)
//                val sVPicture = activity?.findViewById<SurfaceView>(R.id.sVPicture)
//
//                val title = dST.text.toString()
//                val year = dSY.text.toString()
//                val month = dSM.text.toString()
//                val day = dSD.text.toString()
//                val note = eVDiary?.text.toString()
//                val picture = sVPicture?.holder.toString()
//
//                val db = _helper.writableDatabase

                Toast.makeText(activity,"保存しました", Toast.LENGTH_SHORT).show()

            }
            .setNegativeButton("キャンセル") { dialog, id ->

            }

        return builder.create()
    }


    fun getTodayYear(): String {
        val date = Date()
        val format = SimpleDateFormat("yyyy", Locale.getDefault())
        return format.format(date)
    }
    fun getTodayMonth(): String {
        val date = Date()
        val format = SimpleDateFormat("MM", Locale.getDefault())
        return format.format(date)
    }
    fun getTodayDay(): String {
        val date = Date()
        val format = SimpleDateFormat("dd", Locale.getDefault())
        return format.format(date)
    }

}