package com.example.picturediaryapp

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment


class ParretDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val colorSelecter = inflater.inflate(R.layout.dialog_selecter, null)

        builder.setView(colorSelecter)
            .setNegativeButton("キャンセル") {dialog, id ->

            }
        return builder.create()
    }

}