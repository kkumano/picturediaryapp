package com.example.picturediaryapp

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment

class DiaryDeleteDialogFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage("本当に消してもよろしいですか？")
            .setPositiveButton("消去") { dialog, id ->
                //データベースから消去してtopに戻る
                Toast.makeText(activity,"消去しました", Toast.LENGTH_SHORT).show()
            }
            .setNegativeButton("キャンセル") {dialog, id ->

            }

        return builder.create()
    }
}